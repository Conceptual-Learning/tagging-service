const unified = require('unified')
const stream = require('unified-stream')
const parse = require('rehype-parse')
const pos = require('retext-pos')
const keywords = require('retext-keywords')
const rehype2retext = require('rehype-retext')
const English = require('parse-english')
const inspect = require('unist-util-inspect')
const toString = require('nlcst-to-string')

// keywords stringify

function stringify() {
  this.Compiler = compiler;
}

function compiler(tree) {
  const keywords = tree.data.keywords
  .map(keyword => toString(keyword.matches[0].node)).join('\n');
  const keyphrases = tree.data.keyphrases
  .map(phrase => phrase.matches[0].nodes.map(toString).join('')).join('\n');
  const ans = `keywords:\n${keywords}\n\nkeyphrases:\n${keyphrases}\n`
  return ans;
}

processor = unified()
.use(parse, {emitParseErrors: true, duplicateAttribute: false})
.use(rehype2retext, English)
.use(pos) // Make sure to use `retext-pos` before `retext-keywords`.
.use(keywords)
.use(stringify)

process.stdin.pipe(stream(processor)).pipe(process.stdout)
