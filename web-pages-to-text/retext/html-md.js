const unified = require('unified')
const stream = require('unified-stream')
const html = require('rehype-parse')
const rehype2remark = require('rehype-remark')
const markdown = require('remark-stringify')

const processor = unified()
  .use(html)
  .use(rehype2remark)
  .use(markdown)

process.stdin.pipe(stream(processor)).pipe(process.stdout)
