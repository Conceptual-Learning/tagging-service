var router = require('express').Router();

router.get('/', (req, res, next) => {
    res.send('/concepts');
});

module.exports = router;
