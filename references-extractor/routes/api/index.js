var router = require('express').Router();

router.use('/concepts', require('./concepts'));

module.exports = router;
