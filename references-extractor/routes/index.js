var router = require('express').Router();


/* GET home page. */
router.use('/index', function(req, res, next) {
  res.send('/index');
});

router.use('/api', require('./api'));

module.exports = router;
